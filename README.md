# test
## relative path

![image1](image1.png)
![image2](image2.png)

## absolute path

![image1](https://codeberg.org/hw/test-images/raw/branch/master/image1.png)
![image2](https://codeberg.org/hw/test-images/raw/branch/master/image2.png)